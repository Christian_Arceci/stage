<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package travel-buzz
 */

get_header();

travel_buzz_banner();

$travel_buzz_layout = travel_buzz_get_sidebar_layout();

?>

<div class="content">
	<div class="container">
		<div class="inner-wrapper <?php echo esc_attr( $travel_buzz_layout ); ?>">

			<main id="primary" class="site-main">

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'singular' );

					the_tags();

					?>
					<div class="single-page-pagination "> 
					<?php
						the_post_navigation(
							array(
								'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'travel-buzz' ) . '</span> <span class="nav-title">%title</span>',
								'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'travel-buzz' ) . '</span> <span class="nav-title">%title</span>',
							)
						);
					?>
					</div>
					<?php

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</main><!-- #main -->


			<?php get_sidebar(); ?>

		</div><!-- .inner-wrapper -->
	</div><!-- .container -->
</div><!-- .content -->

<?php get_footer(); ?>
