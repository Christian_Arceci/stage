=== Travel Buzz ===

Contributors: WEN Solutions
Theme URI: https://wensolutions.com/themes/travel-buzz/
Demo: https://wptravel.io/demo/?demo=travel-buzz
Documentation : https://wptravel.io/documentation/travel-buzz/
Tags: entertainment, grid-layout, one-column, two-columns, left-sidebar, right-sidebar, custom-background, custom-colors, custom-header, custom-logo, custom-menu, featured-images, footer-widgets, theme-options, threaded-comments, translation-ready
Stable tag: 1.0.3
Tested up to: 5.6
Requires at least: 5.0
Requires PHP: 5.4
License: GNU General Public License v2 or later
License URI: LICENSE

Travel Buzz is an elegant WordPress theme for tour and travel operators.

== Description ==

Travel Buzz is an elegant WordPress theme for tour and travel operators. This theme is suitable for travel agencies, hotels, tour operators, or any other organization related to travel and tours. It has a user-friendly theme options panel based on powerful Customizer API, which makes the theme pretty easy to customize and configure. Most importantly this theme has compatibility for WP Travel plugin and Elementor out of the box, which adds ease to managing tours and bookings. Theme Demo: https://wptravel.io/demo/?demo=travel-buzz, Documentation: https://wptravel.io/documentation/travel-buzz/

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Travel Buzz includes support for Elementor page builder plugin, MailChimp For WordPress, WP Travel, and for Infinite Scroll in Jetpack.

== Changelog ==

= 1.0.3 =
* Fixed compatibility issues with WP Travel

= 1.0.2 =
* Fixed customizer edit bubble
* Fixed accessiblity issues

= 1.0.1 =
* Added Theme URI in readme.txt and stylesheet header.

= 1.0.0 =
* Initial release

== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2020 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2018 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)


* TGM-Plugin-Activation 2.6.1 by Thomas Griffin, Gary Jones, Juliette Reinders Folmer
	Source: https://github.com/TGMPA/TGM-Plugin-Activation
	License: GNU General Public License v2.0
	License Url: https://github.com/TGMPA/TGM-Plugin-Activation/blob/develop/LICENSE.md

* Slick Version - 1.8.0
	Source: https://kenwheeler.github.io/slick/
	License: The MIT License (MIT) copyright (c) 2013-2016
	license Url : https://github.com/kenwheeler/slick/blob/master/LICENSE

* Magnific Popup v1.1.0 by Dmitry Semenov
	Source: https://github.com/dimsemenov/Magnific-Popup
	License: The MIT License (MIT) copyright (c) 2014-2016
	License Url: https://github.com/dimsemenov/Magnific-Popup/blob/master/LICENSE

* Font Awesome 5.11.2 by @fontawesome
	Source: http://fontawesome.io
	License: http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)

	fontawesome-webfont.woff, fontawesome-webfont.woff2, slick.ttf and slick.woff are licensed under Font Awesome 4.6.3 by @davegandy
	License: http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
	Source: http://fontawesome.io

* Purecounter - 1.1.1
	Source: https://github.com/srexi/purecounterjs
	License: The MIT License (MIT) copyright (c) 2019
	license Url : https://github.com/srexi/purecounterjs/blob/master/LICENSE

== Image Credits ==
All images are licensed under (CC0) Creative Common license

* Screenshot:
	Banner Image: [https://pxhere.com/en/photo/494496]

* Assets images listed below are self created:
	- [assets/images/counter.png]
	- [assets/images/placeholder.png]
