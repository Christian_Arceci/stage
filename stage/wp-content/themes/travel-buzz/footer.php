<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$travel_buzz_enable_footer_widget_area = travel_buzz_theme_mod( 'enable_footer_widget_area' );

$travel_buzz_footer_classes   = array();
$travel_buzz_footer_classes[] = 'site-footer';
$travel_buzz_footer_classes[] = "footer-widgets-{$travel_buzz_enable_footer_widget_area}";
$travel_buzz_footer_classes[] = travel_buzz_get_newsletter_form( false ) ? 'newsletter-enable' : 'newsletter-disable';

get_template_part( 'template-parts/footer/scroll-to-top' );
get_template_part( 'template-parts/footer/newsletter' );

?>


<footer id="footer" class="<?php echo esc_attr( implode( ' ', $travel_buzz_footer_classes ) ); ?>">

	<?php
		get_template_part( 'template-parts/footer/footer-widgets' );
		get_template_part( 'template-parts/footer/copyright' );
	?>

</footer><!-- #footer -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>
