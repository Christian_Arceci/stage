<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package travel-buzz
 */

if ( ! defined( 'TRAVEL_BUZZ_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( 'TRAVEL_BUZZ_VERSION', '1.0.0' );
}

if ( ! function_exists( 'travel_buzz_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function travel_buzz_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Travel Buzz, use a find and replace
		 * to change 'travel-buzz' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'travel-buzz', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'travel-buzz' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'travel_buzz_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'travel_buzz_setup' );


if ( ! function_exists( 'travel_buzz_menu_fallback' ) ) {

	/**
	 * If no navigation menu is assigned, this function will be used for the fallback.
	 *
	 * @see https://developer.wordpress.org/reference/functions/wp_nav_menu/ for available $args arguments.
	 * @param  mixed $args Menu arguments.
	 * @return string $output Return or echo the add menu link.
	 */
	function travel_buzz_menu_fallback( $args ) {
		if ( ! current_user_can( 'edit_theme_options' ) ) {
			return;
		}

		$link  = $args['link_before'];
		$link .= '<a href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '" title="' . esc_attr__( 'Opens in new tab', 'travel-buzz' ) . '" target="__blank">' . $args['before'] . esc_html__( 'Add a menu', 'travel-buzz' ) . $args['after'] . '</a>';
		$link .= $args['link_after'];

		if ( false !== stripos( $args['items_wrap'], '<ul' ) || false !== stripos( $args['items_wrap'], '<ol' ) ) {
			$link = "<li class='menu-item'>{$link}</li>";
		}

		$output = sprintf( $args['items_wrap'], $args['menu_id'], $args['menu_class'], $link );

		if ( ! empty( $args['container'] ) ) {
			$output = sprintf( '<%1$s class="%2$s" id="%3$s">%4$s</%1$s>', $args['container'], $args['container_class'], $args['container_id'], $output );
		}

		if ( $args['echo'] ) {
			echo wp_kses_post( $output );
		}

		return $output;

	}
}


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function travel_buzz_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'travel_buzz_content_width', 640 ); // phpcs:ignore WPThemeReview.CoreFunctionality.PrefixAllGlobals.NonPrefixedVariableFound
}
add_action( 'after_setup_theme', 'travel_buzz_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function travel_buzz_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'travel-buzz' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'travel-buzz' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebars(
		4,
		array(
			/* translators: %d is footer widget ID. */
			'name'          => esc_html__( 'Footer Widgets %d', 'travel-buzz' ),
			'id'            => 'footer-widgets',
			'description'   => esc_html__( 'Add footer widget here.', 'travel-buzz' ),
			'before_widget' => '<div id="%1$s" class="wp-widget widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
}
add_action( 'widgets_init', 'travel_buzz_widgets_init' );

/**
 * Convert hexdec color string to rgb(a) string.
 *
 * @link https://mekshq.com/how-to-convert-hexadecimal-color-code-to-rgb-or-rgba-using-php/
 *
 * @param string $color Color in hex value | eg: #ffffff or #fff.
 * @param string $opacity Color opacity for RGBA value. If false provided, it will return RGB value.
 */
function travel_buzz_colors_hex2rgb( $color ) {

	$default = '0,0,0';

	// Return default if no color provided.
	if ( empty( $color ) ) {
		return $default;
	}

	// Sanitize $color if "#" is provided.
	if ( '#' === $color[0] ) {
		$color = substr( $color, 1 );
	}

	// Check if color has 6 or 3 characters and get values.
	if ( strlen( $color ) === 6 ) {
		$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
	} elseif ( strlen( $color ) === 3 ) {
		$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
	} else {
		return $default;
	}

	// Convert hexadec to rgb.
	$rgb = array_map( 'hexdec', $hex );

	$output = implode( ',', $rgb );

	// Return rgb color string.
	return $output;
}


function travel_buzz_css_variables() {
	$primary_color   = travel_buzz_theme_mod( 'primary_color' );
	$secondary_color = travel_buzz_theme_mod( 'secondary_color' );
	?>
	<style id="travel-buzz-css-variables">
		:root {
			--Travel-primary-color: <?php echo esc_attr( $primary_color ); ?>;
			--Travel-primary-rbga: <?php echo esc_attr( travel_buzz_colors_hex2rgb( $primary_color ) ); ?>;
			--Travel-secondary-rbga: <?php echo esc_attr( travel_buzz_colors_hex2rgb( $secondary_color ) ); ?>;
			--Travel-secondary-color: <?php echo esc_attr( $secondary_color ); ?>;
		}
	</style>
	<?php
}

/**
 * Enqueue scripts and styles.
 */
function travel_buzz_scripts() {

	$is_wp_travel_page = travel_buzz_is_wp_travel_page();

	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '.min' : null;

	travel_buzz_css_variables();
	travel_buzz_font_loader();
	wp_enqueue_style( 'travel-buzz-fontawesome', get_template_directory_uri() . '/assets/css/all' . $suffix . '.css', array(), TRAVEL_BUZZ_VERSION );

	if ( ! $is_wp_travel_page ) {
		wp_enqueue_style( 'travel-buzz-slick', get_template_directory_uri() . '/assets/slick/slick.css', array(), TRAVEL_BUZZ_VERSION );
		wp_enqueue_style( 'travel-buzz-slick-theme', get_template_directory_uri() . '/assets/slick/slick-theme.css', array(), TRAVEL_BUZZ_VERSION );
	}

	wp_enqueue_style( 'travel-buzz-magnific-popup', get_template_directory_uri() . '/third-party/magnific-popup/css/magnific-popup' . $suffix . '.css', array(), TRAVEL_BUZZ_VERSION );
	wp_enqueue_style( 'travel-buzz-style', get_stylesheet_uri(), array(), TRAVEL_BUZZ_VERSION );
	wp_style_add_data( 'travel-buzz-style', 'rtl', 'replace' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'travel-buzz-navigation', get_template_directory_uri() . '/assets/js/navigation' . $suffix . '.js', array(), TRAVEL_BUZZ_VERSION, true );
	wp_enqueue_script( 'travel-buzz-purecounter', get_template_directory_uri() . '/third-party/purecounter/purecounter' . $suffix . '.js', array(), TRAVEL_BUZZ_VERSION, true );
	wp_enqueue_script( 'travel-buzz-magnific-popup', get_template_directory_uri() . '/third-party/magnific-popup/js/jquery.magnific-popup' . $suffix . '.js', array(), TRAVEL_BUZZ_VERSION, true );

	if ( ! $is_wp_travel_page ) {
		wp_enqueue_script( 'travel-buzz-slick', get_template_directory_uri() . '/assets/slick/slick.js', array(), TRAVEL_BUZZ_VERSION, true );
	}

	wp_register_script( 'travel-buzz-custom-js', get_template_directory_uri() . '/assets/js/custom' . $suffix . '.js', array(), TRAVEL_BUZZ_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'travel-buzz-custom-js' );
}
add_action( 'wp_enqueue_scripts', 'travel_buzz_scripts' );



function travel_buzz_assets_handle( $word ) {

	if ( ! $word ) {
		return $word;
	}

	$word = strtolower( $word );
	$word = str_replace( array( ' ', '_', ',', '  ' ), '-', $word );
	return $word;
}


/**
 * Returns or prints escaped font family name according to the selected font from customizer.
 */
function travel_buzz_get_font_family( $key, $echo = true ) {

	$fonts = travel_buzz_fonts();

	$font_family_key = travel_buzz_theme_mod( $key );

	$font_family = $key && isset( $fonts[ $font_family_key ] ) ? $fonts[ $font_family_key ] : '"Archivo", sans-serif';

	if ( ! $echo ) {
		return $font_family;
	}

	echo esc_attr( $font_family );

}


function travel_buzz_font_loader() {

	$heading_fonts = travel_buzz_theme_mod( 'heading_fonts' );
	$content_fonts = travel_buzz_theme_mod( 'content_fonts' );

	$heading_font_name = travel_buzz_get_font_family( 'heading_fonts', false );
	$content_font_name = travel_buzz_get_font_family( 'content_fonts', false );

	$heading_font_handle = travel_buzz_assets_handle( "nature-life-font-{$heading_font_name}" );
	$content_font_handle = travel_buzz_assets_handle( "nature-life-font-{$content_font_name}" );

	wp_enqueue_style( $heading_font_handle, "//fonts.googleapis.com/css?family={$heading_fonts}", array(), '1.0.0', 'all' );

	/**
	 * Don't load the same font again.
	 */
	if ( ! wp_style_is( $content_font_handle ) ) {
		wp_enqueue_style( $content_font_handle, "//fonts.googleapis.com/css?family={$content_fonts}", array(), '1.0.0', 'all' );
	}

}

if ( ! function_exists( 'travel_buzz_get_newsletter_form' ) ) {

	/**
	 * Prints the newsletter form.
	 *
	 * @uses mc4wp_show_form() - MailChimp for WordPress plugin
	 * @link https://www.mc4wp.com/
	 *
	 * @param bool $echo Return or print the form html.
	 */
	function travel_buzz_get_newsletter_form( $echo = true ) {

		if ( ! function_exists( 'mc4wp_show_form' ) ) {
			return;
		}

		$form_id = 0;

		ob_start();
		try {
			mc4wp_show_form( $form_id, array(), true );
		} catch ( Exception $e ) {
			echo '';
		}
		$form = ob_get_clean();

		if ( ' ' === $form || ! $form ) {
			return;
		}

		if ( ! $echo ) {
			return $form;
		}

		echo $form; // phpcs:ignore

	}
}


/**
 * Returns true if current page/post is associated with wp travel.
 */
function travel_buzz_is_wp_travel_page() {

	/**
	 * Bail if these functions does not exists.
	 */
	if (
		! function_exists( 'travel_edge_itinerary_is_itinerary' ) ||
		! function_exists( 'travel_buzz_itinerary_is_dashboard_page' ) ||
		! function_exists( 'travel_buzz_itinerary_is_cart_page' ) ||
		! function_exists( 'travel_buzz_itinerary_is_checkout_page' )
	) {
		return;
	}

	return travel_buzz_itinerary_is_itinerary() ||
	travel_buzz_itinerary_is_dashboard_page() ||
	travel_buzz_itinerary_is_cart_page() ||
	travel_buzz_itinerary_is_checkout_page();

}


/**
 * Returns the array of html entities without &# and ; to save it from being rendered in value.
 * Use travel_buzz_render_icons_code() to render the icon codes.
 */
function travel_buzz_icons() {
	return apply_filters(
		'travel_buzz_icons',
		array(
			'default' => __( 'Default', 'travel-buzz' ),
			'8230'    => __( 'Horizontal Ellipsis', 'travel-buzz' ),
			'8608'    => __( 'Right Two Headed Arrow', 'travel-buzz' ),
			'10149'   => __( 'Arrow Curved Down Right', 'travel-buzz' ),
			'10173'   => __( 'Heavy Wedge Tailed Right', 'travel-buzz' ),
			'10170'   => __( 'Teardrop Barbed Right Arrow', 'travel-buzz' ),
		)
	);
}

/**
 * Renders the travel_buzz_icons() provided html entities value.
 *
 * @param string $key Selected html entity code.
 * @param bool   $decode Whether or not to render the html entity icon using `html_entity_decode()`.
 * @param bool   $echo Echo or return the data.
 */
function travel_buzz_render_icons_code( $key, $decode = false, $echo = true ) {

	if ( ! $key || 'default' === $key ) {
		return $key;
	}

	$entity = "&#{$key};";

	if ( $decode ) {
		$entity = html_entity_decode( $entity );
	}

	if ( ! $echo ) {
		return $entity;
	}

	echo $entity; //phpcs:ignore
}

/**
 * Returns sidebar layout for the current page.
 */
function travel_buzz_get_sidebar_layout() {

	$layout_for = 'blogs_archives_layout';
	if ( is_page() ) {
		$layout_for = 'pages_layout';
	}
	if ( is_single() ) {
		$layout_for = 'posts_layout';
	}

	return travel_buzz_theme_mod( $layout_for );
}


function travel_buzz_fonts() {

	$fonts = array(
		'Archivo:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap' => esc_html( 'Archivo' ),
		'Cormorant+Garamond:400,400i,500,500i,600,600i,700,700i&display=swap' => esc_html( 'Cormorant Garamond' ),
		'Caveat:400,700'                                   => esc_html( 'Caveat' ),
		'Dancing+Script:400,700'                           => esc_html( 'Dancing Script' ),
		'Heebo:400,500,700,800'                            => esc_html( 'Heebo' ),
		'Kelly+Slab'                                       => esc_html( 'Kelly Slab' ),
		'Lato:400,400i,700,700i'                           => esc_html( 'Lato' ),
		'Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap' => esc_html( 'Roboto' ),
		'Geo:400,400i|Great+Vibes|Lato:300,400,700&display=swap' => esc_html( 'Geo' ),
		'Poppins:300,400,500,600,700,800,900&display=swap' => esc_html( 'Poppins' ),
		'Montserrat:400,400i,500,500i,600,600i,700,700i,800,800i' => esc_html( 'Montserrat' ),
		'Nunito+Sans:400,400i,600,600i,700,700i'           => esc_html( 'Nunito Sans' ),
		'Open+Sans:400,400i,600,600i,700,700i,800,800i'    => esc_html( 'Open Sans' ),
		'Oswald:400,500,600,700'                           => esc_html( 'Oswald' ),
		'Pacifico'                                         => esc_html( 'Pacifico' ),
		'Public+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap' => esc_html( 'Public Sans' ),
		'Playfair+Display:400,400i,700,700i'               => esc_html( 'Playfair Display' ),
		'Ubuntu:400,400i,500,500i,700,700i'                => esc_html( 'Ubuntu' ),
	);

	return apply_filters( 'travel_buzz_fonts', $fonts );
}


/**
 * Filter the excerpt length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function travel_buzz_excerpt_length( $length ) {
	if ( is_admin() ) {
		return $length;
	}

	$custom_length = travel_buzz_theme_mod( 'excerpt_length' );

	return $custom_length ? (int) $custom_length : $length;
}
add_filter( 'excerpt_length', 'travel_buzz_excerpt_length' );


function travel_buzz_get_taxonomies() {
	if ( ! defined( 'WP_TRAVEL_VERSION' ) ) {
		return;
	}
	return array(
		'travel_locations' => __( 'Trip Locations', 'travel-buzz' ),
		'itinerary_types'  => __( 'Trip Types', 'travel-buzz' ),
		'category'         => __( 'WP Category', 'travel-buzz' ),
	);
}


if ( ! function_exists( 'travel_buzz_get_terms' ) ) {

	/**
	 * This function returns the formated array of terms
	 * for the given taxonomy name.
	 *
	 * @param string $tax_name Taxonomy name. Default is "category".
	 * @param string $key Whether set array key by term slug [term_slug] or by term id [term_id]
	 * @param bool   $hide_empty Takes boolean value, pass true if you want to hide empty terms.
	 * @return array $items Formated array for the dropdown options for the customizer.
	 */
	function travel_buzz_get_terms( $tax_name = 'category', $key = 'term_slug', $hide_empty = true ) {

		if ( empty( $tax_name ) ) {
			return;
		}

		$items = array();
		$terms = get_terms(
			array(
				'taxonomy'   => $tax_name,
				'hide_empty' => $hide_empty,
			)
		);

		if ( ! is_wp_error( $terms ) && is_array( $terms ) && count( $terms ) > 0 ) {
			foreach ( $terms as $term ) {
				$term_name = ! empty( $term->name ) ? $term->name : false;
				if ( 'term_slug' !== $key ) {
					$array_key = ! empty( $term->term_id ) ? $term->term_id : '';
				} else {
					$array_key = ! empty( $term->slug ) ? $term->slug : '';
				}
				if ( $term_name ) {
					$items[ $array_key ] = $term_name;
				}
			}
		}

		return $items;
	}
}


function travel_buzz_get_social_links() {
	return array(
		'facebook'  => esc_html__( 'Facebook', 'travel-buzz' ),
		'twitter'   => esc_html__( 'Twitter', 'travel-buzz' ),
		'instagram' => esc_html__( 'Instagram', 'travel-buzz' ),
		'linkedin'  => esc_html__( 'LinkedIn', 'travel-buzz' ),
	);
}

/**
 * Returns valid social links url.
 * Ignores empty values.
 */
function travel_buzz_get_social_links_url() {
	$social_links = (array) travel_buzz_theme_mod( 'social_links' );
	$social_links = array_filter( $social_links );

	return $social_links;
}

/**
 * Prints social link html.
 */
function travel_buzz_social_links_html() {
	$social_links = travel_buzz_get_social_links_url();

	ob_start();
	if ( is_array( $social_links ) && ! empty( $social_links ) ) {
		foreach ( $social_links as $social_link_key => $social_link_url ) {
			$fa_class = "fab fa-{$social_link_key}";

			if ( 'facebook' === $social_link_key ) {
				$fa_class = "fab fa-{$social_link_key}-square";
			}

			if ( 'linkedin' === $social_link_key ) {
				$fa_class = "fab fa-{$social_link_key}-in";
			}
			?>
			<li class="menu-item" title="<?php echo esc_html( ucwords( $social_link_key ) ); ?>">
				<a href="<?php echo esc_url( $social_link_url ); ?>">
					<i class="<?php echo esc_attr( $fa_class ); ?>"></i>
				</a>
			</li>
			<?php
		}
	}

	$html = ob_get_clean();

	echo $html; // phpcs:ignore
}


if ( ! function_exists( 'travel_buzz_get_itinerary_meta' ) ) {

	/**
	 * Returns the array of wp travel trips meta informations.
	 *
	 * @param array $args Arguments for wp travel meta datas. It accepts the following values:
	 *                    * trip_id        => WP Travel Trip ID, default is get_the_ID().
	 *                    * min_price_sale => If set to 0, it will return sale price for any pricing option.
	 *                    * retrive        => Type of meta that you want to receive, default is all.
	 *                                        > **Other accepted values are:**
	 *                                         > * all ( default ),
	 *                                         > * general,
	 *                                         > * prices,
	 *                                         > * date_and_time,
	 *                                         > * trip_terms,
	 *                                         > * thumbnails,
	 *                    * featured_trips => If passed true, then function will returns
	 *                                        the meta info for featured trips only.
	 *                                        Default is false.
	 *
	 * @return array $wp_travel_meta Array of WP Travel meta data.
	 */
	function travel_buzz_get_itinerary_meta( $args = array() ) {

		$wp_travel_meta = array();

		/**
		 * Bail early if WP Travel plugin is not activated or not exists.
		 * Also here WP_Travel is a function name, not a class name.
		 */
		if ( ! function_exists( 'WP_Travel' ) ) {
			return $wp_travel_meta;
		}

		if ( ! class_exists( 'Travel_Buzz_WP_Travel_Metas' ) ) {
			require_once get_template_directory() . '/inc/classes/class-travel-buzz-wp-travel-metas.php';
		}

		$default = array(
			'trip_id'        => get_the_ID(),
			'min_price_sale' => 1,
			'retrive'        => 'all',
			'featured_trips' => false,
		);
		$args    = wp_parse_args( $args, $default );

		$trip_id        = ! empty( $args['trip_id'] ) ? $args['trip_id'] : false;
		$min_price_sale = $args['min_price_sale'];
		$retrive        = ! empty( $args['retrive'] ) ? $args['retrive'] : 'all';
		$featured_trips = ! empty( $args['featured_trips'] ) ? $args['featured_trips'] : false;

		// Bail early if trip id is empty.
		if ( empty( $trip_id ) ) {
			return $wp_travel_meta;
		}

		// Bail early if provided post id is not WP Travel Itinerary post.
		if ( 'itineraries' !== get_post_type( $trip_id ) ) {
			return $wp_travel_meta;
		}

		$meta_object   = new Travel_Buzz_WP_Travel_Metas( $trip_id );
		$general       = $meta_object->general();
		$prices        = $meta_object->prices( $min_price_sale );
		$date_and_time = $meta_object->date_and_time();
		$trip_terms    = $meta_object->trip_terms();
		$thumbnails    = $meta_object->thumbnails();

		/**
		 * Create the meta array.
		 */
		$itinerary_meta = array(
			'general'       => $general,
			'prices'        => $prices,
			'date_and_time' => $date_and_time,
			'trip_terms'    => $trip_terms,
			'thumbnails'    => $thumbnails,
		);
		$wp_travel_meta = $itinerary_meta;

		/**
		 * If $args['featured_trips'] is passed true then
		 * reset the array and list only featured trips.
		 */
		if ( $featured_trips && 'yes' === $itinerary_meta['general']['is_featured'] ) {
			$wp_travel_meta = array();
			$wp_travel_meta = $itinerary_meta;
		}

		if ( 'all' === $retrive ) {
			return $wp_travel_meta;
		}

		return ! empty( $wp_travel_meta[ $retrive ] ) ? $wp_travel_meta[ $retrive ] : array();
	}
}

require get_template_directory() . '/inc/itinerary-helpers.php';


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/dynamic-assets.php';


/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/elementor/class-travel-buzz-load-elementor-widgets.php';


/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/tgm-plugin/tgmpa-hook.php';


/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

if ( class_exists( 'WP_Travel' ) ) {
	require get_template_directory() . '/wp-travel/wp-travel.php';
}
