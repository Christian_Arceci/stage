<?php

/**
 * Create elementor custom widget.
 *
 * @package travel-buzz.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Travel_Buzz_Elementor_Latest_Trips' ) ) {

	class Travel_Buzz_Elementor_Latest_Trips extends \Elementor\Widget_Base {

		/**
		 * Get widget name.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return string Widget name.
		 */
		public function get_name() {
			return 'travel_buzz_latest_trips';
		}

		/**
		 * Get widget title.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return string Widget title.
		 */
		public function get_title() {
			return __( 'Latest Trips', 'travel-buzz' );
		}

		/**
		 * Get widget categories.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return array Widget categories.
		 */
		public function get_categories() {
			return array( 'travel-buzz' );
		}

		/**
		 * Register oEmbed widget controls.
		 *
		 * Adds different input fields to allow the user to change and customize the widget settings.
		 *
		 * @since 1.0.0
		 * @access protected
		 */
		protected function _register_controls() {
			$this->start_controls_section(
				'latest_trips',
				array(
					'label' => __( 'Latest Trips', 'travel-buzz' ),
					'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
				)
			);

			$this->add_control(
				'section_title',
				array(
					'label' => __( 'Section Title', 'travel-buzz' ),
					'type'  => \Elementor\Controls_Manager::TEXT,
				)
			);

			$this->add_control(
				'content_total_posts',
				array(
					'label'       => __( 'Total Posts', 'travel-buzz' ),
					'type'        => \Elementor\Controls_Manager::NUMBER,
					'description' => __( 'Enter the total number of posts you want to display.', 'travel-buzz' ),
					'min'         => 1,
					'default'     => 6,
				)
			);

			$this->add_control(
				'button_label',
				array(
					'label'   => __( 'Button Label', 'travel-buzz' ),
					'type'    => \Elementor\Controls_Manager::TEXT,
					'default' => esc_html__( 'Explore', 'travel-buzz' ),
				)
			);

			$this->end_controls_section();
		}

		/**
		 * Render the html to view.
		 */
		protected function render() {
			$section_title = $this->get_settings_for_display( 'section_title' );
			$button_label  = $this->get_settings_for_display( 'button_label' );

			$args = array(
				'post_type'      => 'itineraries',
				'post_status'    => 'publish',
				'posts_per_page' => $this->get_settings_for_display( 'content_total_posts' ),
			);

			$the_query = new WP_Query( $args );
			?>

			<div id="latest-trip" class="section">
				<div class="container">
					<div class="wrapper">

						<?php if ( $section_title ) { ?>
							<div class="section-heading">
								<h2 class="section-title"><?php echo esc_html( $section_title ); ?></h2>
							</div>
						<?php } ?>

						<div class="inner-container">
							<div class="grid-collection">
								<?php
								while ( $the_query->have_posts() ) {
									$the_query->the_post();

									$wp_travel_metas = travel_buzz_get_itinerary_meta();

									$ratings_html = ! empty( $wp_travel_metas['general']['ratings_html'] ) ? $wp_travel_metas['general']['ratings_html'] : null;

									// Prices.
									$currency_code = ! empty( $wp_travel_metas['prices']['currency_code'] ) ? $wp_travel_metas['prices']['currency_code'] : false;
									$enable_sale   = ! empty( $wp_travel_metas['prices']['enable_sale'] ) ? $wp_travel_metas['prices']['enable_sale'] : false;
									$regular_price = ! empty( $wp_travel_metas['prices']['regular_price'] ) ? $wp_travel_metas['prices']['regular_price'] : false;
									$trip_price    = ! empty( $wp_travel_metas['prices']['trip_price'] ) ? $wp_travel_metas['prices']['trip_price'] : false; // This will give sales price if sale is enabled.

									$travel_locations = ! empty( $wp_travel_metas['trip_terms']['travel_locations'] ) ? $wp_travel_metas['trip_terms']['travel_locations'] : '';

									?>
									<div class="grid-item">
										<div class="grid-item-wrapper">
											<div class="img-container">
												<?php the_post_thumbnail(); ?>
											</div><!-- img-container -->
											<div class="grid-item-inner-container">
												<?php
												the_title(
													'<div class="item-heading"><div class="post-title"><h3><a href="' . esc_url( get_the_permalink() ) . '">',
													'</a></h3></div></div>'
												);
												?>

												<div class="trip-meta-info">
													<?php
													if ( ! empty( $travel_locations ) ) {
														$travel_location = isset( $travel_locations[0] ) ? $travel_locations[0] : '';
														$location_id     = is_object( $travel_location ) && isset( $travel_location->term_id ) ? $travel_location->term_id : false;
														$location        = is_object( $travel_location ) && isset( $travel_location->name ) ? $travel_location->name : false;

														if ( $location ) {
															?>
															<div class="price-day">
																<span><a href="<?php echo esc_url( get_term_link( $location_id ) ); ?>"><i class="fas fa-map-marker-alt"></i> <?php echo esc_html( $location ); ?></a></span>
															</div>
															<?php
														}
													}

													?>
													<?php echo wp_kses_post( $ratings_html ); ?>
												</div>

												<?php if ( get_the_excerpt() ) { ?>
													<div class="excerpt">
														<?php the_excerpt(); ?>
													</div>
												<?php } ?>

												<div class="grid-item-footer">

													<div class="button">
														<a href="<?php the_permalink(); ?>" class="btn-primary btn-prop own-prop"><?php echo esc_html( $button_label ); ?></a>
													</div>

													<div class="pricing">
														<p>
															<span><?php esc_html_e( 'From', 'travel-buzz' ); ?> <?php echo esc_html( $currency_code . $trip_price ); ?></span>

															<?php if ( $enable_sale ) { ?>
																<span>
																	<del><?php echo esc_html( $currency_code . $regular_price ); ?></del>
																</span>
															<?php } ?>
														</p>
													</div>

												</div>

											</div>
										</div>
									</div><!-- grid-item -->
									<?php
								}
								?>

							</div>
						</div>
					</div>
				</div>
			</div><!-- #tour-packages -->

			<?php
			wp_reset_postdata();
		}
	}
}
