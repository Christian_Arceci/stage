<?php

/**
 * Create elementor custom widget.
 *
 * @package travel-buzz.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use Elementor\Plugin;

if ( ! class_exists( 'Travel_Buzz_Elementor_Banner_Slider' ) ) {

	/**
	 * Create course slider widget.
	 */
	class Travel_Buzz_Elementor_Banner_Slider extends \Elementor\Widget_Base {

		/**
		 * Get widget name.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return string Widget name.
		 */
		public function get_name() {
			return 'travel_buzz_banner_slider';
		}

		/**
		 * Get widget title.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return string Widget title.
		 */
		public function get_title() {
			return __( 'Banner Slider', 'travel-buzz' );
		}

		/**
		 * Get widget categories.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return array Widget categories.
		 */
		public function get_categories() {
			return array( 'travel-buzz' );
		}

		/**
		 * Register oEmbed widget controls.
		 *
		 * Adds different input fields to allow the user to change and customize the widget settings.
		 *
		 * @since 1.0.0
		 * @access protected
		 */
		protected function _register_controls() {
			$this->banner_slider();
			$this->trip_filter();
		}

		/**
		 * Creates banner slider controls.
		 */
		private function banner_slider() {
			$taxonomies = travel_buzz_get_taxonomies();

			$this->start_controls_section(
				'banner_slider',
				array(
					'label' => __( 'Banner Slider', 'travel-buzz' ),
					'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
				)
			);

			/**
			 * If WP Travel is active then display advance category selection option.
			 */
			if ( $taxonomies ) {
				$this->add_control(
					'content_taxonomy',
					array(
						'label'   => __( 'Category Type', 'travel-buzz' ),
						'type'    => \Elementor\Controls_Manager::SELECT2,
						'options' => $taxonomies,
					)
				);

				if ( is_array( $taxonomies ) && ! empty( $taxonomies ) ) {
					foreach ( $taxonomies as $tax_slug => $tax_label ) {
						$this->add_control(
							"content_term_{$tax_slug}",
							array(
								'label'     => $tax_label,
								'type'      => \Elementor\Controls_Manager::SELECT2,
								'options'   => travel_buzz_get_terms( $tax_slug ),
								'condition' => array(
									'content_taxonomy' => $tax_slug,
								),
							)
						);
					}
				}
			} else {
				$this->add_control(
					'content_term_category',
					array(
						'label'   => __( 'Posts Category', 'travel-buzz' ),
						'type'    => \Elementor\Controls_Manager::SELECT2,
						'options' => travel_buzz_get_terms(),
					)
				);
			}

			$this->add_control(
				'content_total_posts',
				array(
					'label'       => __( 'Total Posts', 'travel-buzz' ),
					'type'        => \Elementor\Controls_Manager::NUMBER,
					'description' => __( 'Enter the total number of posts you want to display.', 'travel-buzz' ),
					'min'         => 1,
					'default'     => 5,
				)
			);

			$this->add_control(
				'button_label',
				array(
					'label'   => __( 'Button Label', 'travel-buzz' ),
					'type'    => \Elementor\Controls_Manager::TEXT,
					'default' => esc_html__( 'Read More', 'travel-buzz' ),
				)
			);

			$this->end_controls_section();
		}

		/**
		 * Creates filter controls.
		 */
		private function trip_filter() {
			if ( ! defined( 'WP_TRAVEL_VERSION' ) ) {
				return;
			}

			$this->start_controls_section(
				'trip_filter',
				array(
					'label' => __( 'Trip Filter', 'travel-buzz' ),
					'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
				)
			);

			$this->add_control(
				'trip_filter_is_enable',
				array(
					'label' => __( 'Enable Trip Filter', 'travel-buzz' ),
					'type'  => \Elementor\Controls_Manager::SWITCHER,
				)
			);

			$this->add_control(
				'filter_button_label',
				array(
					'label'     => __( 'Button Label', 'travel-buzz' ),
					'type'      => \Elementor\Controls_Manager::TEXT,
					'default'   => esc_html__( 'Search', 'travel-buzz' ),
					'condition' => array(
						'trip_filter_is_enable' => 'yes',
					),
				)
			);

			$this->end_controls_section();
		}

		/**
		 * HTML content for trip filter content.
		 */
		private function trip_filter_content() {

			if ( ! $this->get_settings_for_display( 'trip_filter_is_enable' ) || ! defined( 'WP_TRAVEL_VERSION' ) ) {
				return;
			}

			$filter_button_label = $this->get_settings_for_display( 'filter_button_label' );
			?>
				<div id="trip-search">
					<div class="container">
						<form action="" class="trip-search-form">
							<div class="form-field">
								<label><i class="fas fa-search"></i> <?php esc_html_e( 'Search', 'travel-buzz' ); ?></label>
								<input type="text" name="s" placeholder="<?php esc_attr_e( 'Type keyword...', 'travel-buzz' ); ?>">
							</div>
							<div class="form-field">
								<label><i class="fas fa-plane-departure"></i> <?php esc_html_e( 'Trip Type', 'travel-buzz' ); ?></label>
								<?php
									$taxonomy_name = 'itinerary_types';
									$args          = array(
										'show_option_all'  => __( 'Trip Types', 'travel-buzz' ),
										'show_option_none' => __( 'Trip Types', 'travel-buzz' ),
										'option_none_value' => null,
										'hide_empty'       => 1,
										'selected'         => 1,
										'hierarchical'     => 1,
										'name'             => $taxonomy_name,
										'id'               => 'trip-type',
										'class'            => 'wp-travel-taxonomy',
										'taxonomy'         => $taxonomy_name,
										'value_field'      => 'slug',
									);
									wp_dropdown_categories( $args, $taxonomy_name );
									?>
							</div>
							<div class="form-field">
								<label><i class="fas fa-map-marker-alt"></i> <?php esc_html_e( 'Location', 'travel-buzz' ); ?></label>
								<?php
								$taxonomy_name = 'travel_locations';
								$args          = array(
									'show_option_all'   => __( 'All Location', 'travel-buzz' ),
									'show_option_none'  => __( 'All Location', 'travel-buzz' ),
									'option_none_value' => null,
									'hide_empty'        => 0,
									'selected'          => 1,
									'hierarchical'      => 1,
									'name'              => $taxonomy_name,
									'id'                => 'destination',
									'class'             => 'wp-travel-taxonomy',
									'taxonomy'          => $taxonomy_name,
									'value_field'       => 'slug',
								);
								wp_dropdown_categories( $args, $taxonomy_name );
								?>
							</div>
							<?php
							if ( $filter_button_label ) {
								?>
								<div class="form-field">
									<button class="search-button"><?php echo esc_html( $filter_button_label ); ?></button>
								</div>
								<?php
							}
							?>
						</form>
					</div>

				</div>
			<?php
		}

		/**
		 * Render the html to view.
		 */
		protected function render() {

			$button_label = $this->get_settings_for_display( 'button_label' );
			$taxonomy     = $this->get_settings_for_display( 'content_taxonomy' );
			$term         = $this->get_settings_for_display( "content_term_{$taxonomy}" );
			$post_type    = defined( 'WP_TRAVEL_VERSION' ) && 'category' !== $taxonomy ? 'itineraries' : 'post';

			$args = array(
				'post_type'      => $post_type,
				'posts_per_page' => $this->get_settings_for_display( 'content_total_posts' ),
			);

			if ( ! empty( $taxonomy ) ) {
				$args['tax_query'] = array( // phpcs:ignore
					array(
						'taxonomy' => $taxonomy,
						'field'    => 'term_id',
						'terms'    => array( $term ),
					),
				);
			}

			$the_query = new WP_Query( $args );
			?>
			<!-- home-page-banner -->
			<div id="home-banner">
				<div class="wrapper">
					<div class="inner-content">
						<div class="banner-slider">
							<?php
							while ( $the_query->have_posts() ) {
								$the_query->the_post();
								?>
								<div <?php post_class(); ?>>
									<div class="background-image img-overlay-5" style="background:url('<?php the_post_thumbnail_url(); ?>');">
										<div class="container">
											<div class="slider-content">
												<?php
												the_title(
													'<div class="main-title"><h1 class="title">',
													'</h1></div>'
												);

												if ( get_the_excerpt() ) {
													?>
													<div class="excerpt">
														<?php the_excerpt(); ?>
													</div>
													<?php
												}

												if ( $button_label ) {
													?>
													<div class="banner-button">
														<a href="<?php the_permalink(); ?>" class="btn-Secondary btn-prop learn-more"><?php echo esc_html( $button_label ); ?></a>
													</div>
													<?php
												}
												?>
											</div><!-- .slider-content -->
										</div><!-- container -->
									</div><!-- background-image -->
								</div><!-- div -->
								<?php
							}
							?>

						</div><!-- .banner-slider -->
					</div><!-- .inner-content -->

				</div>

				<?php

				if ( 'post' !== $post_type ) {
					$this->trip_filter_content();
				}

				?>

			</div><!-- #home-banner -->
			<?php
			wp_reset_postdata();
			$this->custom_scripts();
		}

		/**
		 * Checks if elementor is in editor mode.
		 */
		private function is_editor_mode() {
			return Plugin::$instance->editor->is_edit_mode();
		}

		/**
		 * Custom scripts for this section.
		 */
		private function custom_scripts() {
			if ( ! $this->is_editor_mode() ) {
				return;
			}
			?>
			<script>
				jQuery(function($){
					$('.banner-slider').not('.slick-initialized').slick({
						autoplay: true,
						autoplaySpeed: 7000,
						infinite: true,
						arrows:false,
						dots:true,
						fade: true,
						speed: 500,
						cssEase: 'ease-in-out',
						useTransform: true
					});
				});
			</script>
			<?php
		}
	}
}
