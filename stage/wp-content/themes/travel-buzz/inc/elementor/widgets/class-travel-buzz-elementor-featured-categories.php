<?php

/**
 * Create elementor custom widget.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Travel_Buzz_Elementor_Featured_Categories' ) ) {

	/**
	 * Create course slider widget.
	 */
	class Travel_Buzz_Elementor_Featured_Categories extends \Elementor\Widget_Base {

		/**
		 * Get widget name.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return string Widget name.
		 */
		public function get_name() {
			return 'travel_buzz_featured_categories';
		}

		/**
		 * Get widget title.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return string Widget title.
		 */
		public function get_title() {
			return __( 'Featured Categories', 'travel-buzz' );
		}

		/**
		 * Get widget categories.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return array Widget categories.
		 */
		public function get_categories() {
			return array( 'travel-buzz' );
		}

		/**
		 * Register oEmbed widget controls.
		 *
		 * Adds different input fields to allow the user to change and customize the widget settings.
		 *
		 * @since 1.0.0
		 * @access protected
		 */
		protected function _register_controls() {
			$taxonomies = travel_buzz_get_taxonomies();

			$this->start_controls_section(
				'featured_categories',
				array(
					'label' => __( 'Featured Categories', 'travel-buzz' ),
					'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
				)
			);

			$this->add_control(
				'section_title',
				array(
					'label' => __( 'Title', 'travel-buzz' ),
					'type'  => \Elementor\Controls_Manager::TEXT,
				)
			);

			$this->add_control(
				'section_description',
				array(
					'label' => __( 'Description', 'travel-buzz' ),
					'type'  => \Elementor\Controls_Manager::TEXTAREA,
				)
			);

			$this->add_control(
				'category_icons',
				array(
					'label'       => __( 'Icons', 'travel-buzz' ),
					'label_block' => true,
					'type'        => \Elementor\Controls_Manager::ICONS,
				)
			);

			/**
			 * If WP Travel is active then display advance category selection option.
			 */
			if ( $taxonomies ) {
				$this->add_control(
					'content_taxonomy',
					array(
						'label'   => __( 'Category Type', 'travel-buzz' ),
						'type'    => \Elementor\Controls_Manager::SELECT2,
						'options' => $taxonomies,
					)
				);

				if ( is_array( $taxonomies ) && ! empty( $taxonomies ) ) {
					foreach ( $taxonomies as $tax_slug => $tax_label ) {
						$this->add_control(
							"content_term_{$tax_slug}",
							array(
								'label'     => $tax_label,
								'type'      => \Elementor\Controls_Manager::SELECT2,
								'multiple'  => true,
								'max'       => 6,
								'options'   => travel_buzz_get_terms( $tax_slug ),
								'condition' => array(
									'content_taxonomy' => $tax_slug,
								),
							)
						);
					}
				}
			} else {
				$this->add_control(
					'content_term_category',
					array(
						'label'    => __( 'Posts Category', 'travel-buzz' ),
						'type'     => \Elementor\Controls_Manager::SELECT2,
						'multiple' => true,
						'options'  => travel_buzz_get_terms( 'category' ),
					)
				);
			}

			$this->end_controls_section();
		}

		/**
		 * Render the html to view.
		 */
		protected function render() {
			$title          = $this->get_settings_for_display( 'section_title' );
			$description    = $this->get_settings_for_display( 'section_description' );
			$category_icons = $this->get_settings_for_display( 'category_icons' );
			$category_icons = ! empty( $category_icons['value'] ) ? $category_icons['value'] : '';
			$taxonomy       = $this->get_settings_for_display( 'content_taxonomy' );
			$terms          = $this->get_settings_for_display( "content_term_{$taxonomy}" );

			?>

			<div id="select-destination" class="section">
				<div class="container">
					<div class="wrapper">

					<?php if ( $title || $description ) { ?>
						<div class="section-heading">
							<?php if ( $title ) { ?>
								<h2 class="section-title"><?php echo esc_html( $title ); ?></h2>
							<?php } ?>
							<?php if ( $description ) { ?>
								<p class='heading-excerpt'><?php echo esc_html( $description ); ?></p>
							<?php } ?>
						</div>
					<?php } ?>

						<div class="inner-container">
							<div class="grid-collection">
								<?php
								if ( is_array( $terms ) && ! empty( $terms ) ) {
									foreach ( $terms as $term ) {
										$term_data = get_term_by( 'slug', $term, $taxonomy );

										$term_image_id  = get_term_meta( $term_data->term_id, 'wp_travel_trip_type_image_id', true );
										$term_image_url = $term_image_id ? wp_get_attachment_url( $term_image_id ) : get_template_directory_uri() . '/assets/images/placeholder.png';

										$term_link = get_term_link( $term_data );

										$total_posts = 'category' !== $taxonomy ? $term_data->count . ' ' . esc_html__( 'Tours Available', 'travel-buzz' ) : $term_data->count . ' ' . esc_html__( 'Posts Available', 'travel-buzz' );
										?>
										<div class="grid-item">
											<div class="grid-item-wrapper">
												<a href="<?php echo esc_url( $term_link ); ?>" class="img-container">
													<?php if ( $term_image_url ) { ?>
														
															<img src="<?php echo esc_url( $term_image_url ); ?>">
														
													<?php } ?>
													<div class="grid-item-inner-container">
														<div class="item-heading">
															<div class="post-title">
																<h3>
																	<?php echo esc_html( $term_data->name ); ?>
																</h3>
															</div>
															<span><i class="<?php echo esc_attr( $category_icons ); ?>"></i> <?php echo esc_html( $total_posts ); ?></span>
														</div>
													</div>
												</a><!-- img-container -->
											</div>
										</div><!-- grid-item -->
										<?php
									}
								}
								?>

							</div>
						</div>
					</div>
				</div>
			</div>

			<?php

		}
	}
}
