<?php

/**
 * Create elementor custom widget.
 *
 * @package travel-buzz.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use Elementor\Plugin;

if ( ! class_exists( 'Travel_Buzz_Elementor_Featured_Trips' ) ) {

	class Travel_Buzz_Elementor_Featured_Trips extends \Elementor\Widget_Base {


		/**
		 * Get widget name.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return string Widget name.
		 */
		public function get_name() {
			return 'travel_buzz_featured_trips';
		}

		/**
		 * Get widget title.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return string Widget title.
		 */
		public function get_title() {
			return __( 'Featured Trips', 'travel-buzz' );
		}

		/**
		 * Get widget categories.
		 *
		 * @since 1.0.0
		 * @access public
		 *
		 * @return array Widget categories.
		 */
		public function get_categories() {
			return array( 'travel-buzz' );
		}

		/**
		 * Register oEmbed widget controls.
		 *
		 * Adds different input fields to allow the user to change and customize the widget settings.
		 *
		 * @since 1.0.0
		 * @access protected
		 */
		protected function _register_controls() {
			$this->start_controls_section(
				'featured_trips',
				array(
					'label' => __( 'Featured Trips', 'travel-buzz' ),
					'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
				)
			);

			$this->add_control(
				'section_title',
				array(
					'label' => __( 'Section Title', 'travel-buzz' ),
					'type'  => \Elementor\Controls_Manager::TEXT,
				)
			);

			$this->add_control(
				'section_description',
				array(
					'label' => __( 'Section Description', 'travel-buzz' ),
					'type'  => \Elementor\Controls_Manager::TEXTAREA,
				)
			);

			$this->add_control(
				'content_total_posts',
				array(
					'label'       => __( 'Total Posts', 'travel-buzz' ),
					'type'        => \Elementor\Controls_Manager::NUMBER,
					'description' => __( 'Enter the total number of posts you want to display.', 'travel-buzz' ),
					'min'         => 1,
					'default'     => 6,
				)
			);

			$this->add_control(
				'button_label',
				array(
					'label'   => __( 'Button Label', 'travel-buzz' ),
					'type'    => \Elementor\Controls_Manager::TEXT,
					'default' => esc_html__( 'Book Now', 'travel-buzz' ),
				)
			);

			$this->end_controls_section();
		}

		/**
		 * Render the html to view.
		 */
		protected function render() {

			$section_title       = $this->get_settings_for_display( 'section_title' );
			$section_description = $this->get_settings_for_display( 'section_description' );
			$button_label        = $this->get_settings_for_display( 'button_label' );

			$args = array(
				'post_type'      => 'itineraries',
				'post_status'    => 'publish',
				'posts_per_page' => $this->get_settings_for_display( 'content_total_posts' ),
				'meta_query'     => array(
					array(
						'key'   => 'wp_travel_featured',
						'value' => 'yes',
					),
				),
			);

			$the_query = new WP_Query( $args );
			?>
			<div id="tour-packages" class="section">
				<div class="container">
					<div class="wrapper">

					<?php if ( $section_title || $section_description ) { ?>
						<div class="section-heading">

							<?php if ( $section_title ) { ?>
								<h2 class="section-title"><?php echo esc_html( $section_title ); ?></h2>
							<?php } ?>

							<?php if ( $section_description ) { ?>
								<p class='heading-excerpt'><?php echo esc_html( $section_description ); ?></p>
							<?php } ?>

						</div>
					<?php } ?>

						<div class="inner-container">
							<div class="grid-collection">
								<?php
								while ( $the_query->have_posts() ) {
									$the_query->the_post();

									$wp_travel_metas = travel_buzz_get_itinerary_meta();

									$ratings_html = ! empty( $wp_travel_metas['general']['ratings_html'] ) ? $wp_travel_metas['general']['ratings_html'] : null;

									// Prices.
									$currency_code = ! empty( $wp_travel_metas['prices']['currency_code'] ) ? $wp_travel_metas['prices']['currency_code'] : false;
									$regular_price = ! empty( $wp_travel_metas['prices']['regular_price'] ) ? $wp_travel_metas['prices']['regular_price'] : false;

									$trip_duration_day = ! empty( $wp_travel_metas['date_and_time']['trip_duration_day'] ) ? $wp_travel_metas['date_and_time']['trip_duration_day'] : false;

									?>
									<div class="grid-item">
										<div class="grid-item-wrapper">
											<div class="img-container">
												<?php the_post_thumbnail( 'full' ); ?>
											</div><!-- img-container -->
											<div class="grid-item-inner-container">
												<?php
													the_title(
														'<div class="item-heading"><div class="post-title"><h3><a href="' . esc_url( get_the_permalink() ) . '">',
														'</a></h3></div></div>'
													);
												?>
												<div class="trip-meta-info">

													<?php if ( $regular_price || $trip_duration_day ) { ?>
														<div class="price-day">
															<?php if ( $trip_duration_day ) { ?>
																<span><a href="<?php the_permalink(); ?>" tabindex="-1"><i class="fas fa-suitcase-rolling"></i> <?php echo esc_html( $trip_duration_day ) . ' ' . esc_html__( 'Days', 'travel-buzz' ); ?></a></span>
															<?php } ?>
															<?php if ( $regular_price ) { ?>
																<span><a href="<?php the_permalink(); ?>" tabindex="-1"><?php echo esc_html( $currency_code . $regular_price ); ?></a></span>
															<?php } ?>
														</div>
													<?php } ?>

													<?php echo wp_kses_post( $ratings_html ); ?>

												</div>

												<?php if ( get_the_excerpt() ) { ?>
													<div class="excerpt">
														<?php the_excerpt(); ?>
													</div>
												<?php } ?>

												<?php if ( $button_label ) { ?>

												<div class="grid-item-footer">
													<div class="button">
														<a href="<?php the_permalink(); ?>/#booking" class="btn-primary btn-prop own-prop"><?php echo esc_html( $button_label ); ?></a>
													</div>
												</div>

												<?php } ?>

											</div>
										</div>
									</div><!-- grid-item -->
									<?php
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div><!-- #tour-packages -->

			<?php
			wp_reset_postdata();
		}
	}
}
