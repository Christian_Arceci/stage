<?php
/**
 * Travel Buzz Theme Customizer
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Set default values for the customizer settings.
 */
function travel_buzz_customizer_defaults() {
	$defaults = array(
		'header_type'                => 'scroll',
		'header_opacity'             => 1,
		'primary_color'              => '#014073',
		'secondary_color'            => '#ff8121',
		'scroll_to_top_icon'         => 'default',
		'enable_breadcrumbs'         => 'enable',
		'breadcrumbs_icon'           => 'default',
		'blogs_archives_layout'      => 'right-sidebar',
		'posts_layout'               => 'right-sidebar',
		'pages_layout'               => 'right-sidebar',
		'heading_fonts'              => 'Public+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap',
		'content_fonts'              => 'Public+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap',
		'excerpt_length'             => 55,
		'enable_footer_widget_area'  => 'enable',
		'copyright_text'             => __( 'Copyright © 2020 | Travel Buzz by', 'travel-buzz' ) . ' <a href="' . esc_url( 'https://wensolutions.com/' ) . '">WEN Solutions</a> | ' . __( 'Powered by', 'travel-buzz' ) . ' <a href="' . esc_url( 'https://wordpress.org/' ) . '">WordPress</a>',
		'enable_footer_social_links' => 'enable',
	);
	return apply_filters( 'travel_buzz_customizer_defaults', $defaults );
}


/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function travel_buzz_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'travel_buzz_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'travel_buzz_customize_partial_blogdescription',
			)
		);
	}

	/**
	 * Create panel.
	 */
	$wp_customize->add_panel(
		'travel_buzz_general_options',
		array(
			'title'       => esc_html__( 'General Options', 'travel-buzz' ),
			'description' => '<p>' . esc_html__( 'General options for customizing overall theme appearances.', 'travel-buzz' ) . '</p>',
			'priority'    => 30,
		)
	);
}
add_action( 'customize_register', 'travel_buzz_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function travel_buzz_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function travel_buzz_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function travel_buzz_customize_preview_js() {
	wp_enqueue_script( 'travel-buzz-customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), TRAVEL_BUZZ_VERSION, true );
}
add_action( 'customize_preview_init', 'travel_buzz_customize_preview_js' );

require_once get_template_directory() . '/inc/customizer/includes.php';
