<?php
/**
 * Include customizer related files.
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$travel_buzz_customizer_files = array(
	'/inc/customizer/helpers.php',
	'/inc/customizer/sanitize-callbacks.php',
	'/inc/customizer/active-callbacks.php',
	'/inc/customizer/settings/top-bar.php',
	'/inc/customizer/settings/colors.php',
	'/inc/customizer/settings/header.php',
	'/inc/customizer/settings/social-links.php',
	'/inc/customizer/settings/layouts.php',
	'/inc/customizer/settings/typography.php',
	'/inc/customizer/settings/newsletter.php',
	'/inc/customizer/settings/footer.php',
);

if ( is_array( $travel_buzz_customizer_files ) && ! empty( $travel_buzz_customizer_files ) ) {
	foreach ( $travel_buzz_customizer_files as $travel_buzz_customizer_file ) {
		require_once get_template_directory() . $travel_buzz_customizer_file;
	}
}
