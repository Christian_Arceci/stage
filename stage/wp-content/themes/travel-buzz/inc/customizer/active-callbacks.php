<?php
/**
 * Active callback function.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function travel_buzz_customizer_is_top_bar_active( $control ) {
	$setting = $control->manager->get_setting( 'travel_buzz_theme_mod[enable_top_bar]' )->value();
	return 'enable' === $setting;
}

function travel_buzz_customizer_is_scroll_to_top_active( $control ) {
	$setting = $control->manager->get_setting( 'travel_buzz_theme_mod[enable_scroll_to_top]' )->value();
	return 'enable' === $setting;
}

function travel_buzz_customizer_is_breadcrumb_active( $control ) {
	$setting = $control->manager->get_setting( 'travel_buzz_theme_mod[enable_breadcrumbs]' )->value();
	return 'enable' === $setting;
}

function travel_buzz_customizer_is_newsletter_active( $control ) {
	$setting = $control->manager->get_setting( 'travel_buzz_theme_mod[enable_newsletter]' )->value();
	return 'enable' === $setting;
}
