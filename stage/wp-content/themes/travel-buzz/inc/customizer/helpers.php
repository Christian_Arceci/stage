<?php
/**
 * Customizer helper functions.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



/**
 * Returns customizer settings.
 *
 * @uses get_theme_mod
 */
function travel_buzz_theme_mod( $key ) {

	$mods = get_theme_mod( 'travel_buzz_theme_mod' );

	$defaults = travel_buzz_customizer_defaults();
	$default  = isset( $defaults[ $key ] ) ? $defaults[ $key ] : '';

	return isset( $mods[ $key ] ) ? $mods[ $key ] : $default;

}


/**
 * Creates enable/disable settings for customizer options.
 *
 * @param WP_Customizer_Object $wp_customize Customizer Object.
 * @param array                $args Array arguments for settings.
 * @type [key] Key for default and option name.
 * @type [label] Option label.
 * @type [section_id] Section ID.
 */
function travel_buzz_option_enable_disable( $wp_customize, $args ) {

	$key             = $args['key'];
	$label           = $args['label'];
	$description     = isset( $args['description'] ) ? $args['description'] : '';
	$section_id      = $args['section_id'];
	$active_callback = isset( $args['active_callback'] ) ? $args['active_callback'] : '';

	$defaults = travel_buzz_customizer_defaults();

	$default = isset( $defaults[ $key ] ) ? $defaults[ $key ] : 'disable';

	travel_buzz_register_option(
		$wp_customize,
		array(
			'type'              => 'select',
			'name'              => $key,
			'description'       => $description,
			'default'           => $default,
			'active_callback'   => $active_callback,
			'sanitize_callback' => 'travel_buzz_customizer_sanitize_select',
			'label'             => $label,
			'choices'           => array(
				'enable'  => __( 'Enable', 'travel-buzz' ),
				'disable' => __( 'Disable', 'travel-buzz' ),
			),
			'section'           => $section_id,
		)
	);

}


/**
 * Function to register control and setting. To set defaults, @see `travel_buzz_customizer_defaults();`
 *
 * @uses travel_buzz_customizer_defaults();
 */
function travel_buzz_register_option( $wp_customize, $option ) {

	$key = $option['name'];

	$defaults = travel_buzz_customizer_defaults();
	$default  = isset( $defaults[ $key ] ) ? $defaults[ $key ] : null;

	if ( isset( $option['default'] ) ) {
		$default = $option['default'];
	}

	$key = str_replace( '[', '][', $key );

	$name = "travel_buzz_theme_mod[{$key}]";
	$name = str_replace( ']]', ']', $name );

	// Initialize Setting.
	$wp_customize->add_setting(
		$name,
		array(
			'sanitize_callback' => $option['sanitize_callback'],
			'default'           => $default,
			'transport'         => isset( $option['transport'] ) ? $option['transport'] : 'refresh',
			'theme_supports'    => isset( $option['theme_supports'] ) ? $option['theme_supports'] : '',
		)
	);

	$control = array(
		'label'    => $option['label'],
		'section'  => $option['section'],
		'settings' => $name,
	);

	if ( isset( $option['active_callback'] ) ) {
		$control['active_callback'] = $option['active_callback'];
	}

	if ( isset( $option['priority'] ) ) {
		$control['priority'] = $option['priority'];
	}

	if ( isset( $option['choices'] ) ) {
		$control['choices'] = $option['choices'];
	}

	if ( isset( $option['type'] ) ) {
		$control['type'] = $option['type'];
	}

	if ( isset( $option['input_attrs'] ) ) {
		$control['input_attrs'] = $option['input_attrs'];
	}

	if ( isset( $option['description'] ) ) {
		$control['description'] = $option['description'];
	}

	if ( isset( $option['mime_type'] ) ) {
		$control['mime_type'] = $option['mime_type'];
	}

	if ( ! empty( $option['custom_control'] ) ) {
		$wp_customize->add_control( new $option['custom_control']( $wp_customize, $name, $control ) );
	} else {
		$wp_customize->add_control( $name, $control );
	}
}
