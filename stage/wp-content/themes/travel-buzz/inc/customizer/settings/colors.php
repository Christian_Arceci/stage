<?php
/**
 * Controls and settings
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'travel_buzz_customizer_colors' ) ) {

	function travel_buzz_customizer_colors( $wp_customize ) {

		$section_id = 'colors';

		$color_sets = array(
			'primary_color'   => esc_html__( 'Primary Color', 'travel-buzz' ),
			'secondary_color' => esc_html__( 'Secondary Color', 'travel-buzz' ),
		);

		if ( is_array( $color_sets ) && ! empty( $color_sets ) ) {
			foreach ( $color_sets as $color_set_id => $color_set_label ) {

				travel_buzz_register_option(
					$wp_customize,
					array(
						'type'              => 'color',
						'custom_control'    => 'WP_Customize_Color_Control',
						'name'              => $color_set_id,
						'sanitize_callback' => 'sanitize_hex_color',
						'label'             => $color_set_label,
						'section'           => $section_id,
					)
				);

			}
		}

	}
	add_action( 'customize_register', 'travel_buzz_customizer_colors' );
}
