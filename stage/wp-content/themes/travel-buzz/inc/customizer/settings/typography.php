<?php
/**
 * Controls and settings
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'travel_buzz_customizer_typography' ) ) {

	function travel_buzz_customizer_typography( $wp_customize ) {

		$panel_id   = 'travel_buzz_general_options';
		$section_id = 'travel_buzz_general_options_typography';

		$fonts = travel_buzz_fonts();

		/**
		 * Create section.
		 */
		$wp_customize->add_section(
			$section_id,
			array(
				'title' => __( 'Typography', 'travel-buzz' ),
				'panel' => $panel_id,
			)
		);

		travel_buzz_register_option(
			$wp_customize,
			array(
				'type'              => 'select',
				'name'              => 'heading_fonts',
				'sanitize_callback' => 'travel_buzz_customizer_sanitize_select',
				'label'             => esc_html__( 'Heading Fonts', 'travel-buzz' ),
				'choices'           => $fonts,
				'section'           => $section_id,
			)
		);

	}
	add_action( 'customize_register', 'travel_buzz_customizer_typography' );
}
