<?php
/**
 * Controls and settings
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'travel_buzz_customizer_top_bar' ) ) {

	function travel_buzz_customizer_top_bar( $wp_customize ) {

		$panel_id   = 'travel_buzz_general_options';
		$section_id = 'travel_buzz_general_options_top_bar';

		/**
		 * Create section.
		 */
		$wp_customize->add_section(
			$section_id,
			array(
				'title' => __( 'Top Bar', 'travel-buzz' ),
				'panel' => $panel_id,
			)
		);

		travel_buzz_option_enable_disable(
			$wp_customize,
			array(
				'key'        => 'enable_top_bar',
				'label'      => esc_html__( 'Enable Top Bar', 'travel-buzz' ),
				'section_id' => $section_id,
			)
		);

		$settings = array(
			'top_bar_address' => esc_html__( 'Address', 'travel-buzz' ),
			'top_bar_contact' => esc_html__( 'Contact', 'travel-buzz' ),
			'top_bar_email'   => esc_html__( 'Email', 'travel-buzz' ),
		);

		if ( is_array( $settings ) && ! empty( $settings ) ) {
			foreach ( $settings as $setting_key => $label ) {
				travel_buzz_register_option(
					$wp_customize,
					array(
						'type'              => 'text',
						'name'              => $setting_key,
						'sanitize_callback' => 'sanitize_text_field',
						'active_callback'   => 'travel_buzz_customizer_is_top_bar_active',
						'label'             => $label,
						'section'           => $section_id,
					)
				);
			}
		}

		travel_buzz_option_enable_disable(
			$wp_customize,
			array(
				'key'             => 'enable_top_bar_social_links',
				'label'           => esc_html__( 'Enable Social Links', 'travel-buzz' ),
				'active_callback' => 'travel_buzz_customizer_is_top_bar_active',
				'section_id'      => $section_id,
			)
		);


	}
	add_action( 'customize_register', 'travel_buzz_customizer_top_bar' );
}
