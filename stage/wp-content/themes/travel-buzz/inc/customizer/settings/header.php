<?php
/**
 * Controls and settings
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'travel_buzz_customizer_header' ) ) {

	function travel_buzz_customizer_header( $wp_customize ) {

		$panel_id   = 'travel_buzz_general_options';
		$section_id = 'travel_buzz_general_options_header';

		/**
		 * Create section.
		 */
		$wp_customize->add_section(
			$section_id,
			array(
				'title' => __( 'Header', 'travel-buzz' ),
				'panel' => $panel_id,
			)
		);

		travel_buzz_register_option(
			$wp_customize,
			array(
				'type'              => 'select',
				'name'              => 'header_type',
				'sanitize_callback' => 'travel_buzz_customizer_sanitize_select',
				'label'             => esc_html__( 'Header Type', 'travel-buzz' ),
				'choices'           => array(
					'scroll' => esc_html__( 'Header Scroll', 'travel-buzz' ),
					'sticky' => esc_html__( 'Header Sticky', 'travel-buzz' ),
				),
				'section'           => $section_id,
			)
		);

	}
	add_action( 'customize_register', 'travel_buzz_customizer_header' );
}
