<?php
/**
 * Controls and settings
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'travel_buzz_customizer_social_links' ) ) {

	function travel_buzz_customizer_social_links( $wp_customize ) {

		$panel_id   = 'travel_buzz_general_options';
		$section_id = 'travel_buzz_general_options_social_links';

		/**
		 * Create section.
		 */
		$wp_customize->add_section(
			$section_id,
			array(
				'title' => __( 'Social Links', 'travel-buzz' ),
				'panel' => $panel_id,
			)
		);

		$social_links = travel_buzz_get_social_links();

		if ( is_array( $social_links ) && ! empty( $social_links ) ) {
			foreach ( $social_links as $social_link_key => $social_link_label ) {
				travel_buzz_register_option(
					$wp_customize,
					array(
						'type'              => 'url',
						'name'              => "social_links[{$social_link_key}]",
						'sanitize_callback' => 'esc_url_raw',
						'label'             => $social_link_label,
						'section'           => $section_id,
					)
				);
			}
		}

	}
	add_action( 'customize_register', 'travel_buzz_customizer_social_links' );
}
