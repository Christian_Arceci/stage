<?php
/**
 * Controls and settings
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'travel_buzz_customizer_layouts' ) ) {

	function travel_buzz_customizer_layouts( $wp_customize ) {

		$panel_id   = 'travel_buzz_general_options';
		$section_id = 'travel_buzz_general_options_layouts';

		/**
		 * Create section.
		 */
		$wp_customize->add_section(
			$section_id,
			array(
				'title' => __( 'Layouts', 'travel-buzz' ),
				'panel' => $panel_id,
			)
		);
		$layouts = array(
			'blogs_archives_layout' => esc_html__( 'Blogs/Archives layout', 'travel-buzz' ),
			'posts_layout'          => esc_html__( 'Posts layout', 'travel-buzz' ),
			'pages_layout'          => esc_html__( 'Pages layout', 'travel-buzz' ),
		);

		$choices = array(
			'no-sidebar'    => esc_html__( 'No Sidebar', 'travel-buzz' ),
			'left-sidebar'  => esc_html__( 'Left Sidebar', 'travel-buzz' ),
			'right-sidebar' => esc_html__( 'Right Sidebar', 'travel-buzz' ),
		);

		if ( is_array( $layouts ) && ! empty( $layouts ) ) {
			foreach ( $layouts as $key => $label ) {

				travel_buzz_register_option(
					$wp_customize,
					array(
						'type'              => 'select',
						'name'              => $key,
						'sanitize_callback' => 'travel_buzz_customizer_sanitize_select',
						'label'             => $label,
						'choices'           => $choices,
						'section'           => $section_id,
					)
				);

			}
		}

	}
	add_action( 'customize_register', 'travel_buzz_customizer_layouts' );
}
