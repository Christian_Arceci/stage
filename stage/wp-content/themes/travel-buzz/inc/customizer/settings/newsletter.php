<?php
/**
 * Controls and settings
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'travel_buzz_customizer_newsletter' ) ) {

	function travel_buzz_customizer_newsletter( $wp_customize ) {

		if ( ! travel_buzz_get_newsletter_form( false ) ) {
			return $wp_customize;
		}

		$panel_id   = 'travel_buzz_general_options';
		$section_id = 'travel_buzz_general_options_newsletter';

		/**
		 * Create section.
		 */
		$wp_customize->add_section(
			$section_id,
			array(
				'title' => __( 'Newsletter', 'travel-buzz' ),
				'panel' => $panel_id,
			)
		);

		travel_buzz_option_enable_disable(
			$wp_customize,
			array(
				'key'        => 'enable_newsletter',
				'label'      => esc_html__( 'Enable Newsletter', 'travel-buzz' ),
				'section_id' => $section_id,
			)
		);

		travel_buzz_register_option(
			$wp_customize,
			array(
				'type'              => 'text',
				'name'              => 'newsletter_title',
				'sanitize_callback' => 'sanitize_text_field',
				'active_callback'   => 'travel_buzz_customizer_is_newsletter_active',
				'label'             => esc_html__( 'Newsletter Title', 'travel-buzz' ),
				'section'           => $section_id,
			)
		);

		travel_buzz_register_option(
			$wp_customize,
			array(
				'type'              => 'textarea',
				'name'              => 'newsletter_description',
				'sanitize_callback' => 'sanitize_textarea_field',
				'active_callback'   => 'travel_buzz_customizer_is_newsletter_active',
				'label'             => esc_html__( 'Newsletter Description', 'travel-buzz' ),
				'section'           => $section_id,
			)
		);

	}
	add_action( 'customize_register', 'travel_buzz_customizer_newsletter' );
}
