<?php
/**
 * Controls and settings
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'travel_buzz_customizer_footer' ) ) {

	function travel_buzz_customizer_footer( $wp_customize ) {

		$panel_id   = 'travel_buzz_general_options';
		$section_id = 'travel_buzz_general_options_footer';

		/**
		 * Create section.
		 */
		$wp_customize->add_section(
			$section_id,
			array(
				'title' => __( 'Footer', 'travel-buzz' ),
				'panel' => $panel_id,
			)
		);

		travel_buzz_option_enable_disable(
			$wp_customize,
			array(
				'key'        => 'enable_footer_widget_area',
				'label'      => esc_html__( 'Enable Widget Area', 'travel-buzz' ),
				'section_id' => $section_id,
			)
		);

	}
	add_action( 'customize_register', 'travel_buzz_customizer_footer' );
}
