<?php
/**
 * Dynamic styles and scripts according to customizer settings.
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Dynamic styles and scripts according to customizer settings.
 */
function travel_buzz_dynamic_asset() {
	$styles  = travel_buzz_dynamic_styles();
	$scripts = travel_buzz_dynamic_scripts();

	wp_add_inline_style( 'travel-buzz-style', $styles );
	wp_add_inline_script( 'travel-buzz-custom-js', $scripts );
}
add_action( 'wp_enqueue_scripts', 'travel_buzz_dynamic_asset', 50 );

function travel_buzz_dynamic_styles() {

	ob_start();
	?>
	<style>
		body {
			font-family: '<?php travel_buzz_get_font_family( 'content_fonts' ); ?>';
		}

		h1, h2, h3, h4, h5, h6 {
			font-family: '<?php travel_buzz_get_font_family( 'heading_fonts' ); ?>';
		}

	</style>
	<?php
	$styles = ob_get_clean();
	return str_replace( array( '<style>', '</style>' ), null, $styles );
}


function travel_buzz_dynamic_scripts() {
	ob_start();
	?>
	<script>
		jQuery(function($) {
			<?php if ( 'sticky' === travel_buzz_theme_mod( 'header_type' ) ) { ?>

				/**
				 * =========================
				 * <?php esc_html_e( 'Sticky header', 'travel-buzz' ); ?>
				 * =========================
				 */
				$(window).on('scroll', function() {
					if ($(window).scrollTop() >= 50) {
						$('.heading-content').addClass('is-sticky-header');
					} else {
						$('.heading-content').removeClass('is-sticky-header', 1000, "easeInBack");
					}
				});
				/**
				 * =========================
				 * <?php esc_html_e( 'Sticky header', 'travel-buzz' ); ?>
				 * =========================
				 */

			<?php } ?>
		});
	</script>
	<?php
	$scripts = ob_get_clean();
	return str_replace( array( '<script>', '</script>' ), null, $scripts );
}
