jQuery(function ($) {

  /**
   * =========================
   * Accessibility codes start
   * =========================
   */
  $(document).on('mousemove', 'body', function (e) {
    $(this).removeClass('keyboard-nav-on');
  });
  $(document).on('keydown', 'body', function (e) {
    if (e.which == 9) {
      $(this).addClass('keyboard-nav-on');
    }
  });
  /**
   * =========================
   * Accessibility codes end
   * =========================
   */

  /**
   * =========================
   * mobile navigation codes start
   * =========================
   */

  /* button for subm-menu (work only on mobile) */

  $('#primary-menu')
    .find('li.menu-item-has-children')
    .prepend('<button class="btn_submenu_dropdown"><span><i class="drop-down-icon"></i></span></button>');

  /* submenu toggle */
  $(document).on('click ', '.btn_submenu_dropdown', function () {
    $(this).toggleClass('active');
    $(this).parent().find('.sub-menu').first().slideToggle();
  });


  /* menu toggle */
  var nav_menu = $('.main-navigation ul.nav-menu');
  $(document).on('click ', '.menu-toggle', function () {
    $('.main-navigation').addClass('toggled');
    $(this).toggleClass('menu-toggle--active');
    nav_menu.slideToggle();
  });

  /**
   * =========================
   * mobile navigation codes ended
   * =========================
   */

  /**
  * ============================
  *  gallery masonry
  * ============================
  */


  /*  function masonry(grid) {
     
     var grid = $(grid);
   
     var rowGap = parseInt(grid.css("grid-row-gap"));
     var rowHeight = (rowGap = isNaN(rowGap) ? 16 : rowGap <= 0 ? 1 : rowGap);
   
     grid.css("grid-auto-rows", rowHeight + "px");
     grid.css("grid-row-gap", rowGap + "px"); */

  /*  var itemHeight = $(".post-inner-wraper");
   var innerContentHeight = itemHeight.outerHeight();
   console.log(itemHeight); */

  /*     grid.children().each(function () {
          $(this).css(
              "grid-row-end",
              "span " +
                  (1 + Math.ceil((rowGap + innerContentHeight(this)) / (rowGap + rowHeight)))
          );
      });
    } */

  /*   
    function innerContentHeight(item) {
      var content = $(item).html();
      $(item).html("");
      var temp = $('<div style="padding:0;margin:0">');
      temp.html(content);
      $(item).append(temp);
      var itemHeight = temp.outerHeight();
      temp.remove();
      $(item).append(content);
      return itemHeight;
    } */



  /*   ["load", "resize"].forEach(function (event) {
      $(window).on(event, function () {
          masonry(".has-masonry-type");
      });
    }); */

  /**
    * ============================
    * gallery masonry ended
    * ============================
    */
  /**

  /**
   * =========================
   * scroll up/back to top
   * =========================
   */

  var scroll = $(window).scrollTop();
  var $scroll_btn = $('#btn-scrollup');
  var $scroll_obj = $('.scrollup');
  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 1) {
      $scroll_btn.css({ bottom: "25px" });
    }
    else {
      $scroll_btn.css({ bottom: "-100px" });
    }
  });
  $scroll_obj.click(function () {
    $('html, body').animate({ scrollTop: '0px' }, 800);
    return false;
  });
  /**
   * =========================
   * scroll up/back to top
   * =========================
   */

  /**
  * =========================
  * notice bar
  * =========================
  */
  $(document).on("click", '.notice-bar-cross', function () {
    $(".notice-bar").fadeOut();
  });
  /**
   * =========================
   * noticebar
   * =========================
   */

  /**
  * =========================
  * slick
  * =========================
  */

  function travel_buzz_slick_slider() {

    $(".lazy").slick({
      lazyLoad: 'ondemand', // ondemand progressive anticipated
      infinite: true
    });

    $('.banner-slider').not('.slick-initialized').slick({
      autoplay: true,
      autoplaySpeed: 7000,
      infinite: true,
      arrows: false,
      dots: true,
      fade: true,
      speed: 500,
      cssEase: 'ease-in-out',
      useTransform: true
    });

    $('.testimonial-slider').not('.slick-initialized').slick({
      dots: true,
      autoplay: true,
      infinite: true,
      fade: true,
      speed: 300,
      slidesToShow: 1,
      centerPadding: '40px',
      slidesToScroll: 1
    });
    $('.logo-slider').not('.slick-initialized').slick({
      dots: false,
      infinite: true,
      speed: 800,
      autoplaySpeed: 3000,
      arrows: false,
      autoplay: true,
      slidesToShow: 5,
      centerPadding: '40px',
      slidesToScroll: 1,
      responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 808,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 601,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 359,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });





  }
  travel_buzz_slick_slider();
  /**
  * =========================
  * slick
  * =========================
  */


});
