<?php
/**
 * Template part for displaying page content in single page and posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div class="singular-content-wrapper ">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="post-title">
			<?php
				the_title( '<h2 class="entry-title">', '</h2>' );
			?>
		</div>

		<div class="single-post-meta-info">
			<footer class="entry-footer">
				<?php travel_buzz_entry_footer(); ?>
			</footer><!-- entry-footer -->
		</div><!-- single-post-meta-info -->

		<div class="entry-content">
			<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'travel-buzz' ),
					'after'  => '</div>',
				)
			);
			?>
		</div><!-- .entry-content -->

	</article><!-- #post-<?php the_ID(); ?> -->
</div>
