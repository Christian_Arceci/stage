<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel-buzz
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


?>


<article <?php post_class( 'grid-item' ); ?> id="post-<?php the_ID(); ?>">
	<div class="wrapper">

		<div class="img-container">
			<?php travel_buzz_post_thumbnail(); ?>
			<div class="date-meta">
				<div class="blog-date-wrapper">
					<div class="blog-day">13</div>
					<div class="blog-month">Nov</div>
				</div>
			</div>
		</div><!-- img-container -->

		<div class="inner-content-wrapper">

			<div class="right-content">
				<div class="comment-cat">
					<div class="meta-left">
						<?php
							travel_buzz_entry_category_lists();
						?>
					</div>
					<div class="meta-right">
						<?php
						travel_buzz_posted_by();
						travel_buzz_entry_comment_link();
						?>
					</div>
				</div>

				<header class="entry-header">
					<?php
					if ( is_singular() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif;
					?>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<div class="excerpt">
						<?php the_excerpt(); ?>
					</div>
					<div class="post-button">
						<a class="btn-primary btn-prop own-prop" href="<?php the_permalink(); ?>"><?php esc_html_e( 'Read More', 'travel-buzz' ); ?></a>
					</div>
				</div><!-- .entry-content -->
			</div>
		</div>

	</div><!-- wrapper -->

</article><!-- #post-<?php the_ID(); ?> -->
