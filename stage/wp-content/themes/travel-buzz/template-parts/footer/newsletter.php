<?php
/**
 * Footer partials for newsletter section.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! travel_buzz_get_newsletter_form( false ) ) {
	return;
}

if ( 'enable' !== travel_buzz_theme_mod( 'enable_newsletter' ) ) {
	return;
}

$newsletter_title       = travel_buzz_theme_mod( 'newsletter_title' );
$newsletter_description = travel_buzz_theme_mod( 'newsletter_description' );

?>

<!-- newsletter -->
<div id="newsletter-section" class="section">
	<div class="container">
		<div class="wrapper">

			<?php if ( $newsletter_title || $newsletter_description ) { ?>
			<div class="section-heading">
				<?php if ( $newsletter_title ) { ?>
					<h2 class="section-title"><?php echo esc_html( $newsletter_title ); ?></h2>
				<?php } ?>
				<?php if ( $newsletter_description ) { ?>
					<p class="heading-excerpt"><?php echo esc_html( $newsletter_description ); ?></p>
				<?php } ?>
			</div>
			<?php } ?>

			<div class="content">
				<div class="content-wrapper">
					<?php travel_buzz_get_newsletter_form(); ?>
				</div>
			</div><!-- right-content -->

		</div><!-- wrapper -->
	</div><!-- container -->
</div><!-- newsletter-section -->
