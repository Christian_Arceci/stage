<?php
/**
 * Footer partials for the scroll to top.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div id="btn-scrollup">
	<a class="scrollup" href="#"></a>
</div>
<?php
