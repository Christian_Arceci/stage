<?php
/**
 * Footer partial for copyright.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$copyright_text = travel_buzz_theme_mod( 'copyright_text' );

if ( ! $copyright_text ) {
	return;
}

?>

<div class="footer-bottom ">
	<div class="container">
		<div class="wrapper">
			<div id="colophon" class="site-credit">
				<div class="site-info">
					<?php echo wp_kses_post( wpautop( $copyright_text ) ); ?>
				</div><!-- .site-info -->
			</div><!-- #colophon -->
		</div><!-- wrapper -->
	</div><!-- container -->
</div><!-- .footer-bottom -->
