<?php
/**
 * Footer partial for the widget area.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( 'enable' !== travel_buzz_theme_mod( 'enable_footer_widget_area' ) ) {
	return;
}

?>

<div class="container">
	<div class="footer-widgets-inner">
		<div class="inner-wrapper">
		<?php
		for ( $i = 1; $i <= 4; $i++ ) {
			$sidebar_index = "footer-widgets-{$i}";
			?>
			<aside>
			<?php dynamic_sidebar( $sidebar_index ); ?>
			</aside>
			<?php
		}
		?>
		</div><!-- inner-wrapper -->
	</div><!-- .footer-widgets-inner -->
</div><!-- .container -->
