<?php
/**
 * Header navigation partial.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div class="theme-primary-navigation">
	<nav id="site-navigation" class="main-navigation">
		<div class="toggle-button-wrapper">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
				<span class="menu-toggle__text"><?php esc_html_e( 'Toggle Menu', 'travel-buzz' ); ?></span>
			</button>
		</div>
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
				'fallback_cb'    => 'travel_buzz_menu_fallback',
			)
		);
		?>
	</nav><!-- #site-navigation -->
</div><!-- #theme-primary-navigation -->

