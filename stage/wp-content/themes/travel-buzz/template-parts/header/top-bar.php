<?php

/**
 * Theme Top bar html partial.
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( 'enable' !== travel_buzz_theme_mod( 'enable_top_bar' ) ) {
	return;
}

$top_bar_address = travel_buzz_theme_mod( 'top_bar_address' );
$top_bar_contact = travel_buzz_theme_mod( 'top_bar_contact' );
$top_bar_email   = travel_buzz_theme_mod( 'top_bar_email' );
?>

<div class="header-top-bar">
	<div class="container">
		<div class="wrapper">
			<div class="header-top-content">

				<?php if ( $top_bar_address || $top_bar_contact || $top_bar_email ) { ?>
					<div class="header-top-left-content">
						<ul class="contact-information">

							<?php if ( $top_bar_address ) { ?>
								<li class="list">
									<span class="address">
										<a>
											<i class="fa fa-map-marker" aria-hidden="true"></i>
											<span><?php echo esc_html( $top_bar_address ); ?></span>
										</a>
									</span>
								</li>
							<?php } ?>

							<?php if ( $top_bar_contact ) { ?>
								<li class="list">
									<span class="phone">
										<a>
											<i class="fas fa-phone-alt" aria-hidden="true"></i>
											<span><?php echo esc_html( $top_bar_contact ); ?></span>
										</a>
									</span>
								</li>
							<?php } ?>

							<?php if ( $top_bar_email ) { ?>
								<li class="list">
									<span class="email">
										<a>
											<i class="fas fa-envelope" aria-hidden="true"></i>
											<span><?php echo esc_html( $top_bar_email ); ?></span>
										</a>
									</span>
								</li>
							<?php } ?>

						</ul><!-- .contact-infomation -->
					</div><!-- .header-top-left-content -->
				<?php } ?>

				<?php if ( 'enable' === travel_buzz_theme_mod( 'enable_top_bar_social_links' ) && travel_buzz_get_social_links_url() ) { ?>
					<div class="header-top-right-content">
						<div class="social-navigation-wrapper">
							<div class="site-social">
								<nav class="social-navigation" role="navigation">
									<div class="menu-social-container">
										<ul id="menu-social" class="menu">

											<?php travel_buzz_social_links_html(); ?>

										</ul><!-- #menu-social -->
									</div><!-- .menu-social-container -->
								</nav><!-- .social-navigation -->
							</div><!-- .site-social -->
						</div><!-- .social-navigation-wrapper -->
					</div><!-- header-top-right-content -->
				<?php } ?>

			</div><!-- .header-top-content -->
		</div><!-- .wrapper -->
	</div><!-- .container -->

</div><!-- .header-top-bar -->
