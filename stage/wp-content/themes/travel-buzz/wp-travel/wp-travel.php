<?php
if ( function_exists( 'wptravel_single_excerpt' ) ) {
	remove_action( 'wp_travel_single_trip_after_title', 'wptravel_single_excerpt', 1 );
} elseif ( function_exists( 'wp_travel_single_excerpt' ) ) {
	remove_action( 'wp_travel_single_trip_after_title', 'wp_travel_single_excerpt', 1 );
}

if ( ! function_exists( 'travel_buzz_single_itinerary_header_contents' ) ) {

	/**
	 * Function to add header content before main content single itinerary page.
	 *
	 * @param int $trip_id WP Travel Trip ID.
	 */
	function travel_buzz_single_itinerary_header_contents( $trip_id ) {
		global $wp_travel_itinerary;

		$strings = travel_buzz_itinerary_get_strings();

		/**
		 * Trip metas.
		 */
		$trip_types_list = $wp_travel_itinerary->get_trip_types_list();
		$activity_list   = $wp_travel_itinerary->get_activities_list();
		$group_size      = $wp_travel_itinerary->get_group_size();

		$trip_duration_day   = get_post_meta( $trip_id, 'wp_travel_trip_duration', true );
		$trip_duration_night = get_post_meta( $trip_id, 'wp_travel_trip_duration_night', true );
		$trip_duration_day   = $trip_duration_day ? $trip_duration_day : 0;
		$trip_duration_night = $trip_duration_night ? $trip_duration_night : 0;

		/*
		 * translators: %1$s is trip duration days and %2$s is trip duration night.
		 */
		$duration = sprintf( esc_html__( '%1$s Day(s), %2$s Night(s)', 'travel-buzz' ), esc_html( $trip_duration_day ), esc_html( $trip_duration_night ) );

		$travel_locations = get_the_terms( $trip_id, 'travel_locations' );
		$travel_location  = ! is_wp_error( $travel_locations ) && isset( $travel_locations[0] ) ? $travel_locations[0] : '';

		ob_start();
		?>
		<div class="itinerary-header">
			<?php travel_buzz_banner(); ?>
			<div class="trip-travel-trip-info">
				<div class="container">
					<ul class="listing">

						<?php if ( ! empty( $strings['trip_type'] ) ) { ?>
							<li class="listing-item">
								<div class="travel-info">
									<strong class="title"><?php echo esc_html( $strings['trip_type'] ); ?></strong>
								</div>
								<div class="travel-info">
									<span class="value"><?php echo wp_kses_post( $trip_types_list ); ?>
								</div>
							</li>
						<?php } ?>

						<?php if ( ! empty( $strings['activities'] ) ) { ?>
							<li class="listing-item">
								<div class="travel-info">
									<strong class="title"><?php echo esc_html( $strings['activities'] ); ?></strong>
								</div>
								<div class="travel-info">
									<span class="value"><?php echo wp_kses_post( $activity_list ); ?></span>
								</div>
							</li>
						<?php } ?>

						<?php if ( ! empty( $strings['group_size'] ) ) { ?>
							<li class="listing-item">
								<div class="travel-info">
									<strong class="title"><?php echo esc_html( $strings['group_size'] ); ?></strong>
								</div>
								<div class="travel-info">
									<span class="value">
										<?php
										if ( (int) $group_size && $group_size < 999 ) {
											if ( has_filter( 'wptravel_template_group_size_text' ) ) {
												printf( esc_html( apply_filters( 'wptravel_template_group_size_text', __( '%d pax', 'travel-buzz' ) ) ), esc_html( $group_size ) );
											} else {
												printf( esc_html( apply_filters( 'wp_travel_template_group_size_text', __( '%d pax', 'travel-buzz' ) ) ), esc_html( $group_size ) );
											}
										} else {
											if ( has_filter( 'wptravel_default_group_size_text' ) ) {
												echo esc_html( apply_filters( 'wptravel_default_group_size_text', __( 'No Size Limit', 'travel-buzz' ) ) );
											} else {
												echo esc_html( apply_filters( 'wp_travel_default_group_size_text', __( 'No Size Limit', 'travel-buzz' ) ) );
											}
										}
										?>
									</span>
								</div>
							</li>
						<?php } ?>

						<?php if ( ! empty( $strings['locations'] ) && isset( $travel_location->term_id ) ) { ?>
							<li class=" listing-item ">
								<div class="travel-info">
									<strong class="title"><?php echo esc_html( $strings['locations'] ); ?></strong>
								</div>
								<div class="travel-info">
									<span class="value">
										<span class="wp-travel-locations">
											<a href="<?php echo esc_url( get_term_link( $travel_location->term_id ) ); ?>"><?php echo esc_html( $travel_location->name ); ?></a>
										</span>
									</span>
								</div>
							</li>
						<?php } ?>

						<?php if ( ! empty( $strings['trip_duration'] ) ) { ?>
							<li class="listing-item wp-travel-trip-duration">
								<div class="travel-info">
									<strong class="title"><?php echo esc_html( $strings['trip_duration'] ); ?></strong>
								</div>
								<div class="travel-info">
									<span class="value"><?php echo esc_html( $duration ); ?></span>
								</div>
							</li>
						<?php } ?>

					</ul>
				</div>

			</div>
		</div>
		<?php
		$content = ob_get_contents();
		ob_end_flush();
		return $content;
	}
	add_action( 'wp_travel_before_single_itinerary', 'travel_buzz_single_itinerary_header_contents', 8 );
}
