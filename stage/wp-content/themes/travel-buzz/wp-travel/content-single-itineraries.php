<?php
/**
 * Itinerary Single Contnet Template
 *
 * This template can be overridden by copying it to yourtheme/wp-travel/content-single-itineraries.php.
 *
 * HOWEVER, on occasion wp-travel will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see         http://docs.wensolutions.com/document/template-structure/
 * @author      WenSolutions
 * @package     wp-travel/Templates
 * @since       1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'WP_Travel' ) ) {
	return;
}

global $wp_travel_itinerary;

$trip_id = get_the_ID();

// Get settings.
$settings                 = travel_buzz_itinerary_get_settings();
$enquery_global_setting   = ! empty( $settings['enable_trip_enquiry_option'] ) ? $settings['enable_trip_enquiry_option'] : 'yes';
$global_enquiry_option    = get_post_meta( $trip_id, 'wp_travel_use_global_trip_enquiry_option', true );
$wp_travel_itinerary_tabs = travel_buzz_itinerary_get_frontend_tabs();
$booking_tab              = $wp_travel_itinerary_tabs['booking'];
$display_booknow_btn      = ! empty( $booking_tab['show_in_menu'] ) && 'yes' === $booking_tab['show_in_menu'] ? true : false;

$trip_code = $wp_travel_itinerary->get_trip_code();

do_action( 'wp_travel_before_single_itinerary', get_the_ID() );
if ( post_password_required() ) {
	echo get_the_password_form(); // phpcs:ignore
	return;
}

do_action( 'wp_travel_before_content_start' );
?>

<div id="itinerary-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="itinerary-before-tabs">
		<div class="itinerary-content-left">
			<div class="itinerary-img-container" style="background-image: url(<?php echo esc_url( travel_buzz_itinerary_get_post_thumbnail_url( get_the_ID(), 'large' ) ); ?>)"></div>
		</div><!-- .itinerary-content-left -->

		<div class="itinerary-content-right">
			<div class="content entry-content">
				<div class="wp-travel trip-headline-wrapper clearfix">
					<?php if ( travel_buzz_itinerary_is_sale_enabled( get_the_ID() ) ) : ?>
						<div class="wp-travel-offer">
							<span><?php esc_html_e( 'Offer', 'travel-buzz' ); ?></span>
						</div>
						<?php endif; ?>
							<?php if ( $wp_travel_itinerary->has_multiple_images() ) : ?>
					<?php endif; ?>
					<div class="wp-travel-feature-slide-content featured-detail-section right-plot">
						<div class="right-plot-inner-wrap">
							<?php
								do_action( 'wp_travel_before_single_title', get_the_ID() );
								do_action( 'wp_travel_single_trip_after_title', get_the_ID() );
							?>
							<div class="trip-short-desc">
								<?php the_excerpt(); ?>
							</div>
							<div class="booking-form">
								<div class="wp-travel-booking-wrapper">
									<?php

									if ( ! $global_enquiry_option ) {
										$global_enquiry_option = 'yes';
									}
									if ( 'yes' === $global_enquiry_option ) {
										$enable_enquiry = $enquery_global_setting;
									} else {
										$enable_enquiry = get_post_meta( $trip_id, 'wp_travel_enable_trip_enquiry_option', true );
									}

									if ( $display_booknow_btn ) {
										?>
										<button class="wp-travel-booknow-btn"><?php echo esc_html( apply_filters( 'wp_travel_template_book_now_text', __( 'Book Now', 'travel-buzz' ) ) ); ?></button>
										<?php
									}
									?>
									<?php if ( 'yes' == $enable_enquiry ) : ?>
										<a id="wp-travel-send-enquiries" class="wp-travel-send-enquiries" data-effect="mfp-move-from-top" href="#wp-travel-enquiries">
											<span class="wp-travel-booking-enquiry">
												<span class="dashicons dashicons-editor-help"></span>
												<span>
													<?php esc_html_e( 'Trip Enquiry', 'travel-buzz' ); ?>
												</span>
											</span>
										</a>
									<?php endif; ?>
								</div>
							</div>
							<?php
							if ( 'yes' === $enable_enquiry ) {
								travel_buzz_itinerary_get_enquiries_form();
							}
							?>
							<div class="wp-travel-trip-code">
								<span><?php esc_html_e( 'Trip Code', 'travel-buzz' ); ?> :</span><code><?php echo esc_html( $trip_code ); ?></code>
							</div>

						</div>
					</div>
				</div>
			</div><!-- .content.entry-content -->
		</div><!-- .itinerary-content-right -->
	</div><!-- .itinerary-before-tabs -->

	<?php do_action( 'wp_travel_single_trip_after_header', get_the_ID() ); ?>

</div><!-- #itinerary-<?php the_ID(); ?> -->

<?php do_action( 'wp_travel_after_single_itinerary', get_the_ID() ); ?>
