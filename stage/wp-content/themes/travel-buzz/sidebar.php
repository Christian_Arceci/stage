<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package travel-buzz
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}

$travel_buzz_layout = travel_buzz_get_sidebar_layout();

if ( 'no-sidebar' === $travel_buzz_layout ) {
	return;
}

?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
