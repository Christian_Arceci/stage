### VikBooking Automatic Update | 2021-06-25 10:43:03

```json
{
    "plugin": [
        {
            "item": {
                "id": "w.org\/plugins\/mailchimp-for-wp",
                "slug": "mailchimp-for-wp",
                "plugin": "mailchimp-for-wp\/mailchimp-for-wp.php",
                "new_version": "4.8.6",
                "url": "https:\/\/wordpress.org\/plugins\/mailchimp-for-wp\/",
                "package": "https:\/\/downloads.wordpress.org\/plugin\/mailchimp-for-wp.4.8.6.zip",
                "icons": {
                    "2x": "https:\/\/ps.w.org\/mailchimp-for-wp\/assets\/icon-256x256.png?rev=1224577",
                    "1x": "https:\/\/ps.w.org\/mailchimp-for-wp\/assets\/icon-128x128.png?rev=1224577"
                },
                "banners": {
                    "1x": "https:\/\/ps.w.org\/mailchimp-for-wp\/assets\/banner-772x250.png?rev=1184706"
                },
                "banners_rtl": [],
                "tested": "5.7.2",
                "requires_php": "5.3",
                "compatibility": {},
                "current_version": "4.8.5"
            },
            "result": true,
            "name": "MC4WP: Mailchimp for WordPress",
            "messages": [
                "Aggiornamento del plugin: MC4WP: Mailchimp for WordPress",
                "Download dell'aggiornamento da https:\/\/downloads.wordpress.org\/plugin\/mailchimp-for-wp.4.8.6.zip&#8230;",
                "Estrazione dell&#8217;aggiornamento in corso&#8230;",
                "Installazione dell&#8217;ultima versione in corso&#8230;",
                "Attivazione modalit\u00e0 di manutenzione in corso&#8230;",
                "Rimozione della vecchia versione del plugin&#8230;",
                "Disattivazione modalit\u00e0 di manutenzione in corso&#8230;",
                "Il plugin \u00e8 stato aggiornato con successo."
            ]
        }
    ]
}
```

Does `mailchimp-for-wp` match `vikbooking`?

---

