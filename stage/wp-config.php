<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via web
 * puoi copiare questo file in «wp-config.php» e riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Chiavi Segrete
 * * Prefisso Tabella
 * * ABSPATH
 *
 * * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define( 'DB_NAME', 'stage' );

/** Nome utente del database MySQL */
define( 'DB_USER', 'root' );

/** Password del database MySQL */
define( 'DB_PASSWORD', '' );

/** Hostname MySQL  */
define( 'DB_HOST', 'localhost' );

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'nB)ie )S;LnyXUP@3gIj:i[<E`B{tlUIqdO^iy~cFE*@wl3.F9*7KM9(iuUj<k37' );
define( 'SECURE_AUTH_KEY',  ' `aa4S}/u?0Ou^{r/,(v`,l!8L(aU?um&45AAQB*BW1&2WNW|hYSI,k1>Bvx]^4V' );
define( 'LOGGED_IN_KEY',    'SaAvGb#:<#kIt,A!{BEJtO7)&8)c97rJTpYY=.7g3/Bm5aGpq4-BNgRNo*J5Aw|A' );
define( 'NONCE_KEY',        'c$CRwKp-.)IUc2rG _}0t;3*w2hv@%7*0$Lz1OJu05i_i(AtjAOdTO/b^x>cL:LI' );
define( 'AUTH_SALT',        '{<b(sllR}:cx?usf,=`.6 &^/-vj&)Gi8O.8DY1K(_+IUic+aVip@a{/TEUXO=qi' );
define( 'SECURE_AUTH_SALT', 'yX={h6P-z6X=IEJ }4[U[!rk%,2zB&S($&O3<GYzpE)&+O66Zx+h Qa]c$u48v<?' );
define( 'LOGGED_IN_SALT',   '|t#Fe{.mJQx2#y#;-Q)s_=-v{XwpZ$#(~:3X}.jp`R[Ftw5KjOSSf`<U.bN}ORCi' );
define( 'NONCE_SALT',       'pWfZ4Bp2bjis6csjnSbt?TSM~^uD1IXFiKrS+K]`#:v^{N(g^wjK3[xC&Fbbqh[a' );

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi durante lo sviluppo
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 *
 * Per informazioni sulle altre costanti che possono essere utilizzate per il debug,
 * leggi la documentazione
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
